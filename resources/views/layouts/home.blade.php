
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="site-url" content="{{ url('/') }}">
    <meta name="storage-url" content="{{ url('/') }}">
   <title>{{ config('app.name', 'Campaign') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <!-- Scripts End -->
</head>
<body>
<section>
    @include("layouts.partials.home.header")

    <div id="app">
      <div class="">
        <div class="row">
          <div id="mySidenav" class="sidenav">
            <!--   <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
              <a href="#" class="mySidenav" id="team_list" ><p>Team List</p></a>
              <a href="#" class="mySidenav" id="player_list"><p>Player List</p></a>
              <a href="#" class="mySidenav" id="matcher_list"><p>Matches List</p></a>
              <a href="#" class="mySidenav" id="point_list"><p>Point List</p></a>
          </div>
          <!-- <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span> -->
          @yield('content')
        </div>

        <div class="modal" id="popupModel">
             
        </div>
      </div>  
    </div>
    <!-- Footer Section
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
 @include("layouts.partials.home.footer")
</section>

</body>
</html>
