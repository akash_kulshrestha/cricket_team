	<footer class="footer-bottom-content">
		<div class="container">
		   <div class="footer-width">
			<div class="row">
				<div class="footer-top-link">
				</div>
			</div>
		   </div>	
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="footer-bottom-link">
						<div class="col-md-4 col-sm-6 col-xs-12 lPadding">
							<div class="copyright-text">
								<p>Copyright &copy; {{ date('Y') }} All Right Reserved.</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12 lPadding">
							<b>Created By: Akash Kulshrestha</b>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12 rPadding">
							<div class="term-condition text-right">
								<ul class="list-inline">
									<li><a href="javascript:void(0)" data-toggle="modal" data-target="#term_conditions">Terms & Conditions</a></li>
									<li><a href="javascript:void(0)" data-toggle="modal" data-target="#privacy_policy">Privacy Policy</a></li>
									<li><a href="javascript:void(0)" data-toggle="modal" data-target="#user_aggrement">User Agreement</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

<script src="/Assets/js/jquery-3.5.0.min.js" 
  integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
  crossorigin="anonymous"></script>
  <script src="/Assets/js/core/bootstrap.min.js"></script>
  <script src="/Assets/js/dataTable.min.js"></script>
  <script src="/Assets/js/jquery-ui.min.js"></script>
  <script src="/Assets/js/toastr.js"></script>
<script src="/js/app.js"></script>