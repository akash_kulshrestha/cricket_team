     <div class="row fixturesHeading">
       <div class="col-md-6">Matches fixtures</div>
    <div class="col-md-4"></div>
    <div class="col-md-2">
        <button type="button"
         data-toggle="modal" 
         title="Add New"
         data-target="#popupModel"
         class="btn btn-success fixerform">Add New</button>
    </div>  
           
    </div>
<?php foreach($response as $key=>$val) {
    $first_team = $val->first_team;
    $second_team= $val->second_team;
  ?>

    <div class="row">
        <div class="col-md-12">
             <div class="row games-schedule-title">
                <div class="col-md-11">
                    <h5>{{ $val->stadium}} - {{ date("d-M-Y H:i", strtotime($val->match_date))}}</h5>
                </div>
                <div class="col-md-1">
                    <i class="fas fa-edit fixerform"
                        data-toggle="modal" 
                        title="Edit"
                        data-target="#popupModel"
                       rel="<?=$val->id?>"></i>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="games-schedule-items">
                <div class="games-team">
                    <img src="{{$first_team->logo_uri}}"  class="fixtures_log" alt="{{$first_team->club_state}}">
                   
                </div>
                 <span>{{$first_team->name}}</span>
            </div>
        </div>
        <div class="col-md-2">
            <h4 class="img-circle">VS</h4>
        </div>
        
        <div class="col-md-3">
            <div class="games-schedule-items">
                <div class="games-team">
                    <img src="{{$second_team->logo_uri}}" class="fixtures_log" alt="{{$second_team->club_state}}">
                </div>
                 <span>{{$second_team->name}}</span>
            </div>
        </div>
        <div class="col-md-4">
           <?php  if(!empty($val->winner_id)){  ?>
            <table>
                   <tr>
                       <td><b>Toss winner</b></td><td>:</td><td>
                            @if($val->toss_winner == $first_team->id)
                                {{$first_team->name}}
                            @else
                                {{$second_team->name}}
                            @endif    
                       </td>
                   </tr>
                   <tr>
                       <td><b>Winner Team</b></td><td>:</td><td>
                             @if($val->winner_id == $first_team->id)
                                {{$first_team->name}}
                            @else
                                {{$second_team->name}}
                            @endif   
                       </td>
                   </tr>
                   <tr>
                       <td><b>Win By Run</b></td><td>:</td><td>
                            @if($val->first_team_run-$val->second_team_run > 0)
                                {{$val->first_team_run-$val->second_team_run}}
                            @else
                                {{$val->second_team_run-$val->first_team_run}}
                            @endif  
                       </td>
                   </tr> 
            </table>
        <?php } ?>


        </div>



    </div> 
 </div>   

<?php }?>