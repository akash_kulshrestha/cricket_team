<div class="modal-dialog">
  <button type="button" class="close" data-dismiss="modal" >&times;</button> 
  <div class="modal-content" id='popUpBody'>
    <div class="modal-header">
      <span class="modal-title">
         {{ $response[0]->team->name}} ({{$response[0]->country}})
      </span>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
              <table id="dataTablepopup">
                      <thead>
                          <tr>
                             <th class="text-nowrap"></th>
                              <th class="text-nowrap">Name</th>
                              <th class="text-nowrap">Jersey Number</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php foreach($response as $key=> $res){
                          ?>
                          <tr>
                            <td> 
                              <a>
                               <img class="listimagelogo" src="<?=$res->image_uri?>" alt="N/A" >
                              </a> 
                            </td>
                            <td>{{$res->first_name}} {{$res->last_name}}</td>
                            <td>{{$res->jersey_number}}</td>
                           
                        </tr>
                        <?php 
                       }
                        ?>
                      </tbody>
              </table>
        </div>
    </div>
  </div>
</div>

