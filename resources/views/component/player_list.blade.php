<div class="row fixturesHeading">
    <div class="col-md-6">Players List</div>
    <div class="col-md-4"></div>
    <div class="col-md-2">
        <button type="button"
         data-toggle="modal" 
         title="Add New"
         data-target="#popupModel"
         class="btn btn-success playerform">Add New</button>
    </div>   
</div>
<div class="table-responsive">
      <table id="dataTable">
              <thead>
                  <tr>
                      <th>S.No</th>
                      <th class="text-nowrap">Name</th>
                      <th class="text-nowrap">Profile Image</th>
                      <th class="text-nowrap">Team Name</th>
                      <th class="text-nowrap">Jersey Number</th>
                      <th class="text-nowrap">Country</th>
                       <th class="text-nowrap">Action</th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach($response as $key=> $res){
                  ?>
                  <tr>
                    <td>{{$key+1}}</td>
                    <td >{{$res->first_name}} {{$res->last_name}}</td>
                    <td> 
                      <a>
                       <img class="listimagelogo" src="<?=$res->image_uri?>" alt="N/A" >
                      </a> 
                    </td>
                    <td>{{$res->team?$res->team->name:null}}</td>
                    <td>{{$res->jersey_number}}</td>
                    <td>{{$res->country}}</td>
                     <td>
                      <i class="fas fa-edit playerform"
                          data-toggle="modal" 
                          title="Edit"
                          data-target="#popupModel"
                           rel="<?=$res->id?>"></i>
                      <i class="fas fa-trash-alt todelete" rel="<?=$res->id?>" data="players"></i>
                    </td>
                </tr>
                <?php 
               }
                ?>
              </tbody>
      </table>
</div>