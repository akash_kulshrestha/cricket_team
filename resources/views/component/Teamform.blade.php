<div class="modal-dialog">
  <button type="button" class="close closePopUPp" data-dismiss="modal" >&times;</button> 
  <div class="modal-content" id='popUpBody'>
    <div class="modal-header">
      <span class="modal-title">
         @if(empty($response))
         	Add new Team
         @else
         	Edit Team
         @endif	
      </span>
    </div>
    <div class="modal-body">
    	<form id="teamform">
    	 	<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name <span class="start">*</span></label>
                    <input type="text" class="form-control" placeholder="Team name" name="name" value="{{ ($response) ? $response->name:'' }}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Club State </label>
                    <input type="text" class="form-control" placeholder="club state" name="club_state" value="{{ ($response) ? $response->club_state:'' }}">
                  </div>
                </div>
                 <div class="col-md-6">
                     <div class="form-group">
                        <label>Logo </label>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" name="images[]" accept=".jpg,.png,.jpeg">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                     </div>
                  </div>
                   <div class="col-md-6">
                     <div class="form-group">
                         @if(!empty($response))
                         	<input type="hidden" name="id" value="{{$response->id}}">
                         	 <img class="listimagelogo" src="<?=$response->logo_uri?>" alt="#" >
                         @endif
                     </div>
                  </div>
        	</div>
       		<button type="button" class="btn btn-success team_save">Save</button>
       	</form>
    </div>
  </div>
</div>

