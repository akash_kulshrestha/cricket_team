<div class="row fixturesHeading">
            Teams List
    </div>
<div class="table-responsive">
      <table id="dataTable">
              <thead>
                  <tr>
                      <th>S.No</th>
                      <th class="text-nowrap">Team Name</th>
                      <th class="text-nowrap">Team Points</th>
                     
                  </tr>
              </thead>
              <tbody>
                <?php foreach($response as $key=> $res){?>
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$res->team->name}}</td>
                    <td>{{$res->points}}</td>
                  </tr>
                <?php 
               }
                ?>
              </tbody>
      </table>
</div>