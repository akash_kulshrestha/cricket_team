<div class="row fixturesHeading">
    <div class="col-md-6">Teams List</div>
    <div class="col-md-4"></div>
    <div class="col-md-2">
        <button type="button"
         data-toggle="modal" 
         title="Add New"
         data-target="#popupModel"
         class="btn btn-success teamform">Add New</button>
    </div>   
</div>
<div class="table-responsive">
      <table id="dataTable">
              <thead>
                  <tr>
                      <th>S.No</th>
                      <th class="text-nowrap">Team Logo</th>
                      <th class="text-nowrap">Team Name</th>
                      <th class="text-nowrap">Club State</th>
                      <th class="text-nowrap">Team Points</th>
                      <th class="text-nowrap">Action</th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach($response as $key=> $res){?>
                  <tr>
                    <td>{{$key+1}}</td>
                    <td> 
                      <a href="#"
                        rel="<?=$res->id?>"
                        data-toggle="modal" 
                        title="Player list"
                        data-target="#popupModel"
                        class="teamPlayer"
                        >
                       <img class="listimagelogo" src="<?=$res->logo_uri?>" alt="#" >
                      </a> 
                    </td>
                    <td>{{$res->name}}</td>
                    <td>{{$res->club_state}}</td>
                    <td>{{$res->points ? $res->points->points :null}}</td>
                    <td>
                      <i class="fas fa-edit teamform"
                          data-toggle="modal" 
                          title="Edit"
                          data-target="#popupModel"
                           rel="<?=$res->id?>"></i>
                      <i class="fas fa-trash-alt todelete" rel="<?=$res->id?>" data="teams"></i>
                    </td>
                </tr>
                <?php 
               }
                ?>
              </tbody>
      </table>
</div>