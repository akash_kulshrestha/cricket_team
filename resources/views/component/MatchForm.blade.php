<div class="modal-dialog">
  <button type="button" class="close closePopUPp" data-dismiss="modal" >&times;</button> 
  <div class="modal-content" id='popUpBody'>
    <div class="modal-header">
      <span class="modal-title">
         @if(empty($response))
         	Add new Match
         @else
         	Edit Match
         @endif	
      </span>
    </div>
    <div class="modal-body">
    	<form id="fixerform">
    	 	<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Team 1<span class="start">*</span></label>
                    <select name="first_team_id">
                      @foreach($teams as $team)
                          @if(!empty($response) && $team->id == $response->first_team_id)
                            <option value="{{$team->id}}" selected>{{$team->name}} </option>
                          @else
                            <option value="{{$team->id}}">{{$team->name}} </option>
                         @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Team 2<span class="start">*</span></label>
                    <select name="second_team_id">
                      @foreach($teams as $team)
                          @if(!empty($response) && $team->id == $response->second_team_id)
                            <option value="{{$team->id}}" selected>{{$team->name}} </option>
                          @else
                            <option value="{{$team->id}}">{{$team->name}} </option>
                         @endif
                      @endforeach
                    </select>
                  </div>
                </div>
               

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Match Date <span class="start">*</span></label>
                    <input type="Date" class="form-control" name="match_date" value="{{ ($response) ? date('Y-m-d',strtotime($response->match_date)):'' }}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Match Time<span class="start">*</span></label>
                      <div class="row">
                        <div class="col-md-5">
                          <select name="hour">
                            @for($i=0;$i<=24;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                            @endfor
                          </select>
                        </div>
                        <div class="col-md-2">:</div>
                        <div class="col-md-5">
                           <select name="minute">
                            @for($i=0;$i<=60;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                            @endfor
                          </select>
                        </div>
                      </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label>stadium <span class="start">*</span></label>
                    <input type="text" class="form-control" placeholder="stadium" name="stadium" value="{{ ($response) ? $response->stadium:'' }}">
                  </div>
                </div>

                @if(!empty($response))
                 <input type="hidden" name="id" value="{{$response->id}}">
                  <div class="col-md-6">
                     <div class="form-group">
                       <label>Toss winner</label>
                        <select name="toss_winner">
                           <option value="" >Select Team</option>
                          @foreach($teams as $team)
                              @if($team->id == $response->first_team_id || $team->id == $response->second_team_id)
                                @if($team->id == $response->toss_winner)
                                  <option value="{{$team->id}}" selected>{{$team->name}} </option>
                                @else
                                  <option value="{{$team->id}}">{{$team->name}} </option>
                                @endif
                              @endif
                          @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Winner </label>
                        <select name="winner_id">
                          <option value="" >Select Team</option>
                          @foreach($teams as $team)
                              @if($team->id == $response->first_team_id || $team->id == $response->second_team_id)
                                @if($team->id == $response->winner_id)
                                  <option value="{{$team->id}}" selected>{{$team->name}} </option>
                                @else
                                  <option value="{{$team->id}}">{{$team->name}} </option>
                                @endif
                              @endif
                          @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                       <label>First Team Run </label>
                        <input type="text" class="form-control" placeholder="Run" name="first_team_run" value="{{ ($response) ? $response->first_team_run:'' }}">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                       <label>Second Team Run <span class="start">*</span></label>
                        <input type="text" class="form-control" placeholder="Run" name="second_team_run" value="{{ ($response) ? $response->second_team_run:'' }}">
                     </div>
                  </div>
                @endif
        	</div>
       		<button type="button" class="btn btn-success fixersave">Save</button>
       	</form>
    </div>
  </div>
</div>