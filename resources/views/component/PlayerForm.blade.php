<div class="modal-dialog">
  <button type="button" class="close closePopUPp" data-dismiss="modal" >&times;</button> 
  <div class="modal-content" id='popUpBody'>
    <div class="modal-header">
      <span class="modal-title">
         @if(empty($response))
         	Add new Player
         @else
         	Edit Player
         @endif	
      </span>
    </div>
    <div class="modal-body">
    	<form id="playerform">
    	 	<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>First Name <span class="start">*</span></label>
                    <input type="text" class="form-control" placeholder="First name" name="first_name" value="{{ ($response) ? $response->first_name:'' }}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Last Name </label>
                    <input type="text" class="form-control" placeholder="Last name" name="last_name" value="{{ ($response) ? $response->last_name:'' }}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Team <span class="start">*</span></label>
                    <select name="team_id">
                      @foreach($teams as $team)
                          @if(!empty($response) && $team->id == $response->team_id)
                            <option value="{{$team->id}}" selected>{{$team->name}} </option>
                          @else
                            <option value="{{$team->id}}">{{$team->name}} </option>
                         @endif
                      @endforeach
                    </select>
                  </div>
                </div>

                  <div class="col-md-6">
                  <div class="form-group">
                    <label>Jersey Number <span class="start">*</span></label>
                    <input type="text" class="form-control" placeholder="Jersey Number" name="jersey_number" value="{{ ($response) ? $response->jersey_number:'' }}">
                  </div>
                </div>


                  <div class="col-md-6">
                  <div class="form-group">
                    <label>Country <span class="start">*</span></label>
                    <input type="text" class="form-control" placeholder="country" name="country" value="{{ ($response) ? $response->country:'' }}">
                  </div>
                </div>



                <div class="col-md-6">
                  <div class="form-group">
                    <label>Player History </label>
                    <input type="text" class="form-control" placeholder="History" name="player_history" value="{{ ($response) ? $response->player_history:'' }}">
                  </div>
                </div>


              
                 <div class="col-md-6">
                     <div class="form-group">
                        <label>Image </label>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" name="images[]" accept=".jpg,.png,.jpeg">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                     </div>
                  </div>
                   <div class="col-md-6">
                     <div class="form-group">
                         @if(!empty($response))
                         	<input type="hidden" name="id" value="{{$response->id}}">
                         	 <img class="listimagelogo" src="<?=$response->image_uri?>" alt="#" >
                         @endif
                     </div>
                  </div>
        	</div>
       		<button type="button" class="btn btn-success player_save">Save</button>
       	</form>
    </div>
  </div>
</div>

