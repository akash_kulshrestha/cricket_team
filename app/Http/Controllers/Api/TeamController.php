<?php
namespace App\Http\Controllers\Api;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Exception;
use \Validator;
use App\Models\Team;
use App\Models\Player;
use App\Models\Matche;
use App\Models\Point;

use Illuminate\Validation\Rule;
use App\Traits\Response;
use App\Traits\MatchesMaker;

class TeamController extends \App\Http\Controllers\Controller
{   
   use Response,MatchesMaker;
   public function teamList(Request $request)
   {
       try{
            $inputs = $request->all();
            $TeamObj  = new Team();
            $response =$TeamObj->getTeam($inputs);
            return view('component.team_table',compact("response"));
           // return $this->success($this->output($response),200);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   }


   public function playerLlist(Request $request)
   {
       try{
            $inputs = $request->all();
            $PlayerObj  = new Player();
            $response =$PlayerObj->getPlayers($inputs);
            return view('component.player_list',compact("response"));
          //  return $this->success($this->output($response),200);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   }

    public function teamPlayer(Request $request)
    {
       try{
            $PlayerObj  = new Player();
            $response =$PlayerObj->getPlayers($request);
            return view('component.player_list_popup',compact("response"));
          //  return $this->success($this->output($response),200);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   }

   public function matchesList(Request $request)
    {
       try{
            $MatcheObj  = new Matche();
            $response =$MatcheObj->getMatche($request);
            return view('component.Matches',compact("response"));
          //  return $this->success($this->output($response),200);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   }

   public function pointList(Request $request)
    {
       try{
            $PointObj  = new Point();
            $response =$PointObj->getPoints($request);
            return view('component.Points',compact("response"));
          //  return $this->success($this->output($response),200);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   }


   public function teamForm(Request $request)
    {
       try{
            $TeamObj  = new Team();
            $response =$TeamObj->findTeam($request);
            return view('component.Teamform',compact("response"));
          //  return $this->success($this->output($response),200);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   }

   public function teamSave(Request $request)
    {
       try{
            $rules = [
                'name'=>'required|unique:teams'
            ];
            if(!empty($request->id)){
              $rules['name'] =  ['required', Rule::unique("teams")->ignore($request->id)];
            }

            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $TeamObj  = new Team();
            $imagepast = $this->uploadAttechments($request,"teamlogo");
            if(!empty($imagepast[0])){
              $request["logo_uri"] =$imagepast[0];
            }
            $response =$TeamObj->saveTeam($request);
           return $this->apiResponse($response);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   } 


   public function playerForm(Request $request)
    {
       try{
            $PlayerObj  = new Player();
            $response =$PlayerObj->findPlayers($request);
            $TeamObj  = new Team();
            $teams =$TeamObj->getTeam("");
            return view('component.PlayerForm',compact("response" ,"teams"));
          //  return $this->success($this->output($response),200);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   } 

    public function fixerForm(Request $request)
    {
       try{
            $MatcheObj  = new Matche();
            $response =$MatcheObj->FindMatche($request);
            $TeamObj  = new Team();
            $teams =$TeamObj->getTeam("");
            return view('component.MatchForm',compact("response" ,"teams"));
          //  return $this->success($this->output($response),200);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   } 


   


 public function playerSave(Request $request)
    {
       try{
            $rules = [
                'first_name'=>'required',
                "team_id"=>'required|numeric',
                "country"=>'required',
                "jersey_number"=>'required|numeric',
            ];
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $PlayerObj  = new Player();
            $imagepast = $this->uploadAttechments($request,"teamlogo");
            if(!empty($imagepast[0])){
              $request["image_uri"] =$imagepast[0];
            }
            $response =$PlayerObj->savePlayer($request);
            return $this->apiResponse($response);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   } 

   public function fixerSave(Request $request)
    {
       try{
            $rules = [
                'first_team_id'=>'required|numeric',
                "second_team_id"=>'required|numeric',
                "match_date"=>'required',
                "hour"=>'required|numeric',
                "minute"=>'required|numeric',
                "stadium"=>'required'
            ];
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $MatcheObj  = new Matche();
            $response =$MatcheObj->saveMatch($request);
            return $this->apiResponse($response);
       }
       catch (\Throwable $e) {
           return $this->failure($e);
       }
   } 






   public function toDelete(Request $request){
        DB::table($request->table)->where("id",$request->id)->delete();
        return $this->success($this->output(["message"=>"delete successfully"]),200);
   }   


   


}

