<?php

namespace App\Traits;

use Exception;
use Illuminate\Http\Request;
use App\Mail\DefaultMail;
use App\Mail\AllTypeMail;
use App\Traits\ResponseCodes;

trait Response {

	use ResponseCodes;

	/**
	* @var Object
	*/
	private $responseData;

	/**
	* @var Array
	*/
	private $responseCode;

    /**
     * Prepares response format for APIs and AJAX requests.
     * @param Exception|Array;
     * @return Array;
     */
	public function success ($arguments, $code = 200)
	{
        if (json_decode($arguments)) $this->responseData['data'] = json_decode($arguments);
        else $this->responseData['data'] = ['message' => $arguments];

        $this->responseData['code'] = $this->getStatusCodes($code);
        $this->responseCode = $code;

        return $this->sendResponse();
	}

    /**
     * Prepares response format for APIs and AJAX requests.
     * @param Exception|Array;
     * @return Array;
     */
	public function failure ($arguments, Int $code = 500)
	{
      
        if(!empty($arguments->getCode())) $code=$arguments->getCode();

        if (json_decode($arguments->getMessage())) $this->responseData['error'] = json_decode($arguments->getMessage());
        elseif(!empty($arguments->getMessage())) 
             $this->responseData['error'] = ['message' => $arguments->getMessage()];
        else
            $this->responseData['error'] = ['message' => $arguments];

        $this->responseData['code'] = $this->getStatusCodes($code);
        $this->responseCode = $code;
        if(is_array($this->responseData['code']))
            return $this->sendResponse(); 
        echo $arguments->getMessage(); 
        exit;
	}  

    /**
     * Prepares response format for APIs and AJAX requests.
     */
	public function output ($arguments)
	{
        return json_encode($arguments);
	}

    /**
     * Returns the response format.
     * @return Array;
     */
	private function sendResponse ()
	{
        return response($this->responseData, $this->responseCode);
    }

    protected function xmlToArray($result)
	{
		// dd($result);
		if(empty($result))
			return $result;
		$response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml = new \SimpleXMLElement($response);
		$body = isset($xml->xpath('//soapBody')[0])?$xml->xpath('//soapBody')[0]:array();
		$body=json_decode(json_encode((array)$body), TRUE);
		return $body;
    }

  
    /**
     * SEND MAIL FUNCTIONS
     */
    public function sendDefaultEmail( $user, $subject='Travel', $message='Testing' , $pdf1='',$pdf1Name='',$pdf2='',$pdf2Name=''){
        try{
		    \Mail::to( $user['email'] ) //Mail::to( $content['email'] )//Mail::to( 'akash.kulshreshtha@enukesoftware.com' )
	    	->send( new DefaultMail ($user, $subject, $message, $pdf1 , $pdf1Name, $pdf2, $pdf2Name));
        }
        catch (\Throwable $e) {
           return $e->getMessage();
        }
    }

    public function sendAllTypeMail( $subject, $data, $user, $pdf1='',$pdf1Name='',$pdf2='',$pdf2Name=''){
        try{
		    \Mail::to( $user->email )
	    	->send( new  AllTypeMail($user, $subject, $data, $pdf1 , $pdf1Name, $pdf2, $pdf2Name));
        }
        catch (\Throwable $e) {
           return $e->getMessage();
        }
    } 
    

    public function sendMail($template_path,$subject, $emailData, $user){
        try{
            \Mail::send($template_path, ["data"=> $emailData, "user"=>  $user], function($message) use ($user) {
                $message->to($user['email'], $user['first_name'])->subject($subject);
                $message->from('no-reply@goifare.com','Testing API');
            });
            return true;
        }
        catch (\Throwable $e) {
           return $e->getMessage();
        }
    }


}