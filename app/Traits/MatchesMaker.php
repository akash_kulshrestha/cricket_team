<?php

namespace App\Traits;

use Exception;
use App\Models\Discount;

trait MatchesMaker {

    public function CalculatePoints(){

    }

	public function uploadAttechments($request,$file_type){
		$file_url=[];
		 $destinationPath = public_path(); // upload path
        $path = ['full_path'=>'http://'.$_SERVER['SERVER_NAME']];            

		 foreach ($request->allFiles('images') as $file) {
	        foreach ($file as $fkey=> $fileobj) {
	            $profileImage = date('YmdHis') .$fkey. "." . $fileobj->getClientOriginalExtension();
	            $extension = $fileobj->getClientOriginalExtension();
	            
	            if($extension == 'png' || $extension == 'gif' || $extension == 'jpg' || $extension == 'jpeg')
	            {
	                $fileobj->move($destinationPath.'/uploads/Image/'.$file_type.'/', $profileImage);
	                $file_url[] ='/uploads/Image/'.$file_type.'/'.$profileImage;

	            }
	           
	        }
     	}
     	return $file_url;
	}
}