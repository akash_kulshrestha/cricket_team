<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    public $timestamps = false;


    public $fillable = ['name','logo_uri','club_state'];
    
	public function points()
    {
        return $this->hasOne('App\Models\Point','team_id','id');
    }

    public function players()
    {
      return $this->hasMany('App\Models\Player','team_id','id');
    }

    public function findTeam($request){
        if(!empty($request->team_id)){
            return self::find($request->team_id);
        }
        return [];
    }    

    public function getTeam($request){
        if(!empty($request->team_id)){
            return self::find($request->team_id);
        }
		return self::with("points")->get();
    }


    public function saveTeam($request){
        $insertData=[];
        if(!empty($request->id)){
            $id =$request->id;
            $insertData = self::find($id );
        }

        foreach ($this->fillable as $key => $value) {
            if(isset($request[$value])) {
                $insertData[$value]=$request[$value];
            }
        }
        // if(!empty($request->logo_uri)){
        //         $insertData["logo_uri"]=$request->logo_uri;
        // }

        if(!empty($id )){
            self::where(['id'=>$id ])->update($insertData->toArray());
        }else{
            $id = self::create($insertData )->id;
        }
        return ["id"=>$id] ;
    }

}
