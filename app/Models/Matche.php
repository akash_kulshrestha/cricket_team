<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matche extends Model
{
    public $timestamps = false;

    public $fillable = ['first_team_id', 'second_team_id', 'toss_winner', 'winner_id', 'first_team_run', 'second_team_run', 'match_date', 'stadium'];
  
    public function first_team()
    {
      return $this->hasOne('App\Models\Team','id','first_team_id');
    }

    public function second_team()
    {
      return $this->hasOne('App\Models\Team',"id",'second_team_id');
    }

   	public function getMatche($request){
  		return self::select("matches.*")->with("first_team","second_team")->
      join("teams as t1", "t1.id","=",'first_team_id')->
      join("teams as t2", "t2.id","=",'second_team_id')->
      orderBy("match_date")->get();
    }

    public function FindMatche($request){
      if($request && $request->match_id)
           return  self::find($request->match_id);
        return [];
    }


    public function saveMatch($request){
        $insertData=[];
        if(!empty($request->id)){
            $id =$request->id;
            $insertData = self::find($id );
        }
        foreach ($this->fillable as $key => $value) {
            if(isset($request[$value])) {
                $insertData[$value]=$request[$value];
            }
        }
        $insertData["match_date"]=$insertData["match_date"] ." ".$request["hour"].":".$request["minute"].":00";
        if(!empty($id )){
            self::where(['id'=>$id ])->update($insertData->toArray());
        }else{
            $id = self::create($insertData )->id;
        }
        return ["id"=>$id] ;
    }
    
}
