<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Point extends Model
{

	 public function team()
    {
        return $this->hasOne('App\Models\Team',"id",'team_id');
    } 

    public function getPoints($request){
    	$obj = self::with("team")->
          join("teams as t1", "t1.id","=",'team_id');
    	if($request && $request->team_id)
			$obj->where("team_id",$request->team_id);
		return $obj->orderBy("team_id")->get();
    }
}
