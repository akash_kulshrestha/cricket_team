<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public $timestamps = false;


    public $fillable = ['jersey_number', 'first_name', 'last_name', 'team_id', 'image_uri', 'player_history', 'country'];


    public function team()
    {
        return $this->hasOne('App\Models\Team',"id",'team_id');
    }

    public function getPlayers($request){
    	$obj = self::select("players.*")->with("team")->
          join("teams as t1", "t1.id","=",'team_id');
    	if($request && $request->team_id)
			$obj->where("team_id",$request->team_id)->orderBy("jersey_number" ,"ASC");
        if($request && $request->player_id)
           return  $obj->find($request->player_id);
		return $obj->orderBy("team_id")->get();
    }

    public function findPlayers($request){
        if($request && $request->player_id)
           return  self::find($request->player_id);
        return [];
    }

    public function savePlayer($request){
        $insertData=[];
        if(!empty($request->id)){
            $id =$request->id;
            $insertData = self::find($id );
        }
        foreach ($this->fillable as $key => $value) {
            if(isset($request[$value])) {
                $insertData[$value]=$request[$value];
            }
        }
        if(!empty($id )){
            self::where(['id'=>$id ])->update($insertData->toArray());
        }else{
            $id = self::create($insertData )->id;
        }
        return ["id"=>$id] ;
    }

    
}
