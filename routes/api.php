<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/team_list', 'TeamController@teamList');
Route::post('/team_player', 'TeamController@teamPlayer');

Route::post('/player_list', 'TeamController@playerLlist');
Route::post('/point_list', 'TeamController@pointList');

Route::post('/matches_list', 'TeamController@matchesList');
Route::post('/team_point', 'TeamController@teamPoint');

Route::post('/todelete', 'TeamController@toDelete');

Route::post('/teamform', 'TeamController@teamForm');
Route::post('/team_save', 'TeamController@teamSave');

Route::post('/playerform', 'TeamController@playerForm');
Route::post('/player_save', 'TeamController@playerSave');

Route::post('/fixerform', 'TeamController@fixerForm');
Route::post('/fixersave', 'TeamController@fixerSave');

