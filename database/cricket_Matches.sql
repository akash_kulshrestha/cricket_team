-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 01, 2020 at 11:28 PM
-- Server version: 5.7.30-0ubuntu0.16.04.1
-- PHP Version: 7.3.20-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cricket_Matches`
--

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `first_team_id` int(11) NOT NULL,
  `second_team_id` int(11) NOT NULL,
  `toss_winner` int(11) DEFAULT NULL,
  `winner_id` int(11) DEFAULT NULL,
  `first_team_run` int(11) DEFAULT NULL,
  `second_team_run` int(11) DEFAULT NULL,
  `match_date` datetime DEFAULT NULL,
  `stadium` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`id`, `first_team_id`, `second_team_id`, `toss_winner`, `winner_id`, `first_team_run`, `second_team_run`, `match_date`, `stadium`) VALUES
(1, 1, 2, 1, 2, 250, 260, '2020-07-15 00:00:00', 'Holkar Cricket Stadium'),
(2, 4, 5, NULL, NULL, NULL, NULL, '2020-08-29 00:00:00', 'Wankhede Stadium'),
(3, 3, 6, NULL, NULL, NULL, NULL, '2020-08-25 00:00:00', 'Eden Gardens'),
(4, 7, 8, 8, 8, 300, 302, '2020-08-02 00:00:00', 'Saurashtra Cricket Association Stadium');

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `jersey_number` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `team_id` int(11) NOT NULL,
  `image_uri` text,
  `player_history` text,
  `country` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `jersey_number`, `first_name`, `last_name`, `team_id`, `image_uri`, `player_history`, `country`) VALUES
(109, '4', 'Irfan', 'Khan', 1, 'http://cricapi.com/playerpic/32685.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(110, '5', 'Ravichandran', 'Ashwin', 2, 'http://cricapi.com/playerpic/26421.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(111, '6', 'Mithali', 'Dorai', 3, 'http://cricapi.com/playerpic/54273.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(112, '7', 'Jhulan', 'Nishit', 4, 'https://cricapi.com/playerpic/53932.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(113, '8', 'Harmanpreet', 'Kaur', 5, 'https://cricapi.com/playerpic/372317.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(114, '9', 'Milind', 'Tandon', 6, 'https://cricapi.com/playerpic/969381.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(115, '10', 'Manoj', 'Kumar', 7, 'https://cricapi.com/playerpic/35565.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(116, '11', 'Jaydev', 'Dipakbhai', 8, 'http://cricapi.com/playerpic/390484.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(117, '12', 'Washington', 'Sundar', 1, 'http://cricapi.com/playerpic/719715.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(118, '13', 'Ankit', 'Ramdas', 1, 'http://cricapi.com/playerpic/327123.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(119, '14', 'Khaleel', 'Khursheed', 2, 'http://cricapi.com/playerpic/942645.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(120, '15', 'Murugan', 'Ashwin', 3, 'https://cricapi.com/playerpic/528067.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(121, '1', 'Shreyas', 'Santosh', 4, 'http://cricapi.com/playerpic/642519.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(122, '2', 'Chama', 'V', 5, 'http://cricapi.com/playerpic/604616.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(123, '3', 'Mohammed', 'Shami', 6, 'http://cricapi.com/playerpic/481896.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(124, '4', 'Pratyush', 'Singh', 7, 'https://cricapi.com/playerpic/717205.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(125, '5', 'Navdeep', 'Amarjeet', 8, 'https://cricapi.com/playerpic/700167.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(126, '6', 'Shashank', 'Singh', 1, 'https://cricapi.com/playerpic/377534.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(127, '7', 'Jayant', 'Yadav', 1, 'http://cricapi.com/playerpic/447587.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(128, '8', 'Virat', 'Kohli', 2, 'http://cricapi.com/playerpic/253802.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(129, '9', 'Sandeep', 'Sharma', 3, 'https://cricapi.com/playerpic/438362.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(130, '10', 'Ambati', 'Thirupathi', 4, 'http://cricapi.com/playerpic/33141.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(131, '11', 'Abhimanyu', 'Mithun', 5, 'http://cricapi.com/playerpic/310958.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(132, '12', 'Piyush', 'Pramod', 6, 'https://cricapi.com/playerpic/32966.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(133, '13', 'Siddhesh', 'Dinesh', 7, 'https://cricapi.com/playerpic/422342.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(134, '14', 'Harpreet', 'Singh', 8, 'http://cricapi.com/playerpic/340854.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(135, '15', 'Irfan', 'Khan', 1, 'http://cricapi.com/playerpic/32685.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(136, '1', 'Ankit', 'Soni', 1, 'http://cricapi.com/playerpic/1083128.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(137, '2', 'Ravichandran', 'Ashwin', 2, 'http://cricapi.com/playerpic/26421.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(138, '3', 'Mithali', 'Dorai', 3, 'http://cricapi.com/playerpic/54273.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(139, '4', 'Ekta', 'Bisht', 4, 'http://cricapi.com/playerpic/442048.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(140, '5', 'Rajeshwari', 'Shivanand', 5, 'https://cricapi.com/playerpic/709635.jpg', 'Taunton. The middle-order bat now has the second highest score in women\'s Test cricket, having been surpassed by Kiran B\'', 'india'),
(141, '6', 'Jhulan', 'Nishit', 6, 'https://cricapi.com/playerpic/53932.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(142, '7', 'Mansi', 'Joshi', 7, 'http://cricapi.com/playerpic/960815.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(143, '8', 'Harmanpreet', 'Kaur', 8, 'https://cricapi.com/playerpic/372317.jpg', 'If the BCCI were to institute a \'Hall of Fame\' for women\'s cricket in India, Jhulan Goswami would certainly feature. A decade and a half since bursting onto the scene, Goswami, one of the fastest bowlers in the women\'s game till not too long ago, has reap', 'india'),
(144, '9', 'Veda', 'Krishnamurthy', 1, 'https://cricapi.com/playerpic/442205.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(145, '10', 'Smriti', 'Shriniwas', 1, 'https://cricapi.com/playerpic/597806.jpg', 'Harmanpreet Kaur lives and swears by her idol Virender Sehwag\'s mantra of \'see ball, hit ball.\' She represents the new-age India women\'s cricketer, part of a generation that has been at the center of ad campaigns, endorsements and central contracts. She\'s', 'india'),
(146, '11', 'Mona', 'Rajesh', 2, 'http://cricapi.com/playerpic/490624.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(147, '12', 'Shikha', 'Subas', 3, 'https://cricapi.com/playerpic/442145.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(148, '13', 'Poonam', 'Yadav', 4, 'https://cricapi.com/playerpic/630972.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(149, '14', 'Nuzhat', 'Masih', 5, 'http://cricapi.com/playerpic/960973.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(150, '15', 'Punam', 'Ganesh', 6, 'https://cricapi.com/playerpic/360401.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(151, '1', 'Deepti', 'Bhagwan', 7, 'http://cricapi.com/playerpic/597811.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(152, '2', 'Sushma', 'Verma', 8, 'http://cricapi.com/playerpic/597821.jpg', 'with a staggering 214 against England in the second and final Test at', 'india'),
(153, '3', 'Kannaur', 'Lokesh', 1, 'https://cricapi.com/playerpic/422108.jpg', 'with a staggering 214 against England in the second and final Test at', 'india');

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

CREATE TABLE `points` (
  `id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `points` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `team_id`, `points`) VALUES
(1, 1, '62122'),
(2, 2, '69805'),
(3, 3, '84668'),
(4, 4, '25194'),
(5, 5, '826238'),
(6, 6, '9708'),
(7, 7, '253'),
(8, 8, '60');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `logo_uri` text,
  `club_state` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `logo_uri`, `club_state`) VALUES
(1, 'Mumbai Indians', 'Mumbai.png', 'Mumbai'),
(2, 'Rising Pune Supergiant', 'Pune.jpeg', 'Pune'),
(3, 'Kolkata Knight Riders', 'Kolkata.png', 'Kolkata'),
(4, 'Royal Challengers Bangalore', 'Bangalore.png', 'Bangalore'),
(5, 'Gujarat Lions', 'Gujarat.jpeg', 'Gujarat'),
(6, 'Delhi Daredevils', 'Delhi.png', 'Delhi'),
(7, 'Kings XI Punjab', 'Punjab.jpeg', 'Punjab'),
(8, 'Sunrisers Hyderabad', 'Hyderabad.png', 'Hyderabad');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `points`
--
ALTER TABLE `points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
