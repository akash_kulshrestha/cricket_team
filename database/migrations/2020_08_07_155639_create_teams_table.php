<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('logo_uri')->nullable();
            $table->string('club_state')->nullable();
            $table->timestamps();
        });

         DB::table('teams')->insert([
           ['name'=>'Mumbai Indians', 'logo_uri'=>'/uploads/images/teamlogo/Mumbai.png', 'club_state'=>'Mumbai'],
            ['name'=>'Rising Pune Supergiant', 'logo_uri'=>'/uploads/images/teamlogo/Pune.jpeg', 'club_state'=>'Pune'],
            ['name'=>'Kolkata Knight Riders', 'logo_uri'=>'/uploads/images/teamlogo/Kolkata.png', 'club_state'=>'Kolkata'],
            ['name'=>'Royal Challengers Bangalore', 'logo_uri'=>'/uploads/images/teamlogo/Bangalore.png', 'club_state'=>'Bangalore'],
            ['name'=>'Gujarat Lions', 'logo_uri'=>'/uploads/images/teamlogo/Gujarat.jpeg', 'club_state'=>'Gujarat'],
            ['name'=>'Delhi Daredevils', 'logo_uri'=>'/uploads/images/teamlogo/Delhi.png', 'club_state'=>'Delhi'],
            ['name'=>'Kings XI Punjab', 'logo_uri'=>'/uploads/images/teamlogo/Punjab.jpeg', 'club_state'=>'Punjab'],
            ['name'=>'Sunrisers Hyderabad', 'logo_uri'=>'/uploads/images/teamlogo/Hyderabad.png', 'club_state'=>'Hyderabad']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
