<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id');
            $table->integer('jersey_number');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('image_uri')->nullable();
            $table->string('player_history')->nullable();
            $table->string('country')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
