<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('first_team_id');
            $table->integer('second_team_id');
            $table->integer('toss_winner')->nullable();
            $table->integer('winner_id')->nullable();
            $table->integer('first_team_run')->nullable();
            $table->integer('second_team_run')->nullable();
            $table->dateTime('match_date')->nullable();
            $table->string('stadium')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
