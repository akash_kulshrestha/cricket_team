<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id');
            $table->integer('points');
            $table->timestamps();
        });
        
         DB::table('points')->insert([
                ["team_id"=> 1,"points"=> 62122],
                ["team_id"=> 2,"points"=> 69805],
                ["team_id"=> 3,"points"=> 84668],
                ["team_id"=> 4,"points"=> 25194],
                ["team_id"=> 5,"points"=> 826238],
                ["team_id"=> 6,"points"=> 9708],
                ["team_id"=> 7,"points"=> 253],
                ["team_id"=> 8,"points"=> 60]
         ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points');
    }
}
