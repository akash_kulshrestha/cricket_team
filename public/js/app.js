
$(document).ready(function(){

$("#team_list").click(function(){
  $(".mySidenav").removeClass("mySidenavActive");
  $(this).addClass("mySidenavActive");
	ajaxCallHtmlupdatelisting("team_list",{},'post',$(".maindata"))
});

$("#player_list").click(function(){
  $(".mySidenav").removeClass("mySidenavActive");
  $(this).addClass("mySidenavActive");
	ajaxCallHtmlupdatelisting("player_list",{},'post',$(".maindata"))
});

$("#matcher_list").click(function(){
 $(".mySidenav").removeClass("mySidenavActive");
  $(this).addClass("mySidenavActive");
	ajaxCallHtmlupdatelisting("matches_list",{},'post',$(".maindata"))
});

$("#point_list").click(function(){
 $(".mySidenav").removeClass("mySidenavActive");
  $(this).addClass("mySidenavActive");
  ajaxCallHtmlupdatelisting("point_list",{},'post',$(".maindata"))
});


$(document).on("click",".teamPlayer",function(){
  ajaxCallHtmlupdatelisting("team_player",{team_id:$(this).attr("rel")},'post',$("#popupModel"))
})



$(document).on("click",".teamform",function(){
  ajaxCallHtmlupdatelisting("teamform",{team_id:$(this).attr("rel")},'post',$("#popupModel"))
})

$(document).on("click",".playerform",function(){
  ajaxCallHtmlupdatelisting("playerform",{player_id:$(this).attr("rel")},'post',$("#popupModel"))
})

$(document).on("click",".fixerform",function(){
  ajaxCallHtmlupdatelisting("fixerform",{match_id:$(this).attr("rel")},'post',$("#popupModel"))
})




$(document).on("click",".team_save",function(){
   event.preventDefault();
   var form = $('#teamform')[0];
   var data = new FormData(form);
   ajaxCallMultipartForm("team_save",data,$("#team_list"));
})

$(document).on("click",".player_save",function(){
   event.preventDefault();
   var form = $('#playerform')[0];
   var data = new FormData(form);
   ajaxCallMultipartForm("player_save",data,$("#player_list"));
})

$(document).on("click",".fixersave",function(){
   event.preventDefault();
   var form = $('#fixerform')[0];
   var data = new FormData(form);
   ajaxCallMultipartForm("fixersave",data,$("#matcher_list"));
})


$(document).on("click",".todelete",function(){
   event.preventDefault();
   var refreshon=""
   if($(this).attr("data")=="teams"){
     refreshon="#team_list";
   }else{
     refreshon="#player_list";
   }
   data={
     id:$(this).attr("rel"),
     table:$(this).attr("data")
    };
    if(confirm("Are you sure to delete?")){
        ajaxCallHtmlupdatelisting("todelete",data);
        toastr.error("deleted successfully");
        $(refreshon).trigger("click");
    }
})

 function loaddatatable(res){
 	$('#dataTablepopup').DataTable(); 
	$('#dataTable').DataTable(); 
 }

 function ajaxCallHtmlupdatelisting(url,data,method='post',inputfield=""){  
 	  var baseApiUrl = "/api/v1/";
 	  url= baseApiUrl+url;
    $.ajax({
             url: url,
             type: method,
             data: data,
             success: function(response)
             { 
               
               if(inputfield!=""){
                    $(inputfield).html("");
                    $(inputfield).html(response);
                     loaddatatable(data);
               }else{
              
               }
             }, 
             error: function(XMLHttpRequest, textStatus, errorThrown) { 
                activebuttons();
             }  
     });
     
    }
});


function ajaxCallMultipartForm(url, data,clickon="") {
  var response = [];
  var baseApiUrl = "/api/v1/";
   url= baseApiUrl+url;
  $.ajax({
      type: "POST",
      enctype: 'multipart/form-data',
      url: url,
      data: data,
      processData: false,
      contentType: false,
      async: true,
      cache: false,
      timeout: 600000,  
      success: function(data) {

          if(data.success)
          {
              if(clickon !="" ){
                toastr.success("Done successfully");
                 $(".closePopUPp").trigger("click");
                 $(clickon).trigger("click");
              }
           // window.location.reload();
          }else{
            hendleError(data);
          }
      },
      error: function(e) {
          console.log("ERROR : ", e);
          hendleError(e.responseJSON);
      }
  });
  return response;
}


function hendleError(data) {

  $('.errormess').remove();
  if (Object.prototype.toString.call(data.error) == '[object String]' && data.error) {
    toastr.error(data.error);
  } else {
      console.log(data.error);
      $.each(data.error, function(key, value) {
          // value= value.replace("_"," ");

          if ($('select[name=' + key + ']').length)
              object = $('select[name=' + key + ']');
          else if ($('textarea[name=' + key + ']').length)
              object = $('textarea[name=' + key + ']');
          else
              object = $('input[name=' + key + ']');


          object.addClass('errorRed');
          object.after("<span class='errormess' style='width:100%'>" + value + " </span>")
      });
  }
}

 document.getElementById("mySidenav").style.width = "20%";
  $(".maindata").css("width","75%")
  $(".maindata").css("margin-left","22%")
